package main

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
)

const (
	prefixPath     = "├───"
	rootPath       = "│"
	emptyPath      = ""
	lastPrefixPath = "└───"
)

type MyPath struct {
	IsDir  bool
	Name   string
	Size   int64
	Childs []MyPath
}

func buildTree(path string, printFiles bool) ([]MyPath, error) {

	tree := make([]MyPath, 0)
	fileHandler, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer fileHandler.Close()
	fileInfo, err := fileHandler.Stat()
	if err != nil {
		return nil, err
	}
	if !fileInfo.IsDir() {
		return nil, fmt.Errorf("Given path is not dir")
	}
	fileInfos, err := fileHandler.Readdir(0)
	if err != nil {
		return nil, err
	}
	for _, val := range fileInfos {

		switch {
		case val.IsDir():
			newPath := filepath.Join(path, val.Name())
			childs, _ := buildTree(newPath, printFiles)
			tree = append(tree, MyPath{
				IsDir:  true,
				Name:   val.Name(),
				Childs: childs,
			})
		case !val.IsDir() && printFiles:
			tree = append(tree, MyPath{
				IsDir:  false,
				Name:   val.Name(),
				Size:   val.Size(),
				Childs: nil,
			})
		}
	}
	if len(tree) == 0 {
		return nil, nil
	}
	return tree, nil
}

func printTree(out io.Writer, tree []MyPath, indent string) error {

	for idx, val := range tree {
		if idx == len(tree)-1 {
			if val.IsDir {
				fmt.Fprintf(out, "%s%s%s\n", indent, lastPrefixPath, val.Name)
				if val.Childs != nil {
					printTree(out, val.Childs, indent+"\t")
				}
			} else if val.Size == 0 {
				fmt.Fprintf(out, "%s%s%s (empty)\n", indent, lastPrefixPath, val.Name)
			} else {
				fmt.Fprintf(out, "%s%s%s (%db)\n", indent, lastPrefixPath, val.Name, val.Size)
			}
		} else {
			if val.IsDir {
				fmt.Fprintf(out, "%s%s%s\n", indent, prefixPath, val.Name)
				if val.Childs != nil {
					printTree(out, val.Childs, indent+rootPath+"\t")
				}
			} else if val.Size == 0 {
				fmt.Fprintf(out, "%s%s%s (empty)\n", indent, prefixPath, val.Name)
			} else {
				fmt.Fprintf(out, "%s%s%s (%db)\n", indent, prefixPath, val.Name, val.Size)

			}

		}
	}
	return nil
}

func dirTree(out io.Writer, path string, printFiles bool) error {
	tree, err := buildTree(path, printFiles)
	if err != nil {
		return err
	}

	printTree(out, tree, "")
	return nil
}

func main() {
	out := os.Stdout
	if !(len(os.Args) == 2 || len(os.Args) == 3) {
		panic("usage go run main.go . [-f]")
	}
	path := os.Args[1]
	printFiles := len(os.Args) == 3 && os.Args[2] == "-f"
	err := dirTree(out, path, printFiles)
	if err != nil {
		panic(err.Error())
	}
}
